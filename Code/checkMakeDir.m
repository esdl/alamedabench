function [dirName, newDirs] = checkMakeDir(dirName)
% checks for the existance of the full path
% creating directories on the way
% also switches correct backspace for OS
% returns corrected path name and number of directories created

% initialize output
newDirs = 0;

% make correct path name for OS
if (ispc)
    bS = '\';
    sInd = strfind(dirName, '/');
    dirName(sInd) = bS;
else
    bS = '/';
    sInd = strfind(dirName, '\');
    dirName(sInd) = bS;
end

% check and create directories in path
sInd = strfind(dirName, bS);
nDirs = numel(sInd);
for nD = 1:nDirs
    dName = dirName(1:sInd(nD)-1);
    if (~exist(dName, 'dir'))
        mkdir(dName);
        newDirs = newDirs+1;
    end
end
