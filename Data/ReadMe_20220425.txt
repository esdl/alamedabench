This is an explanation of the columns in the csv file called alameda_data_for_lstm_prms_sacsma_ihacres_20210314.csv



Column 1 is the date
1) date: date 



Columns 2-5 are observed Arroyo Hondo subbasin variables
2) obs_precip_AH: observed precipitation (inches) in Arroyo Hondo subbasin
3) obs_tmaxc_AH: observed maximum temperature (degrees Celsius) in Arroyo Hondo subbasin
4) obs_tminc_AH: observed minimum temperature (degrees Celsius) in Arroyo Hondo subbasin
5) obs_flow_AH: observed streamflow (cfs) in Arroyo Hondo subbasin


Columns 6-21 are simulated Arroyo Hondo subbasin variables from an uncalibrated PRMS model
6) prms_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
7) prms_uncalib_sim_actet_AH: simulated actual ET (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
8) prms_uncalib_sim_potet_AH: simulated potential ET (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
9) prms_uncalib_sim_swrad_AH: simulated shortwave radiation (Langleys) from uncalibrated PRMS model in Arroyo Hondo subbasin
10) prms_uncalib_sim_interflow_AH: simulated interflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
11) prms_uncalib_sim_sroff_AH: simulated surface runoff (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
12) prms_uncalib_sim_gwflow_AH: simulated groundwater flow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
13) prms_uncalib_sim_pref_flow_stor_AH: simulated storage (inches) in preferential flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
14) prms_uncalib_sim_slow_stor_AH: simulated storage (inches) in gravity flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
15) prms_uncalib_sim_soil_moist_AH: simulated storage (inches) in capillary flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
16) prms_uncalib_sim_soil_moist_tot_AH: total simulated soil zone storage (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
17) prms_uncalib_sim_hru_intcpstor_AH: simulated interception storage (inches) in the canopy from uncalibrated PRMS model in Arroyo Hondo subbasin
18) prms_uncalib_sim_hru_storage_AH: total simulated storage (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
19) prms_uncalib_sim_gwres_stor_AH: simulated storage (inches) in groundwater reservoir from uncalibrated PRMS model in Arroyo Hondo subbasin
20) prms_uncalib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
21) prms_uncalib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin


Columns 22-37 are simulated Arroyo Hondo subbasin variables from a calibrated PRMS model
22) prms_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
23) prms_calib_sim_actet_AH: simulated actual ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
24) prms_calib_sim_potet_AH: simulated potential ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
25) prms_calib_sim_swrad_AH: simulated shortwave radiation (Langleys) from calibrated PRMS model in Arroyo Hondo subbasin
26) prms_calib_sim_interflow_AH: simulated interflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
27) prms_calib_sim_sroff_AH: simulated surface runoff (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
28) prms_calib_sim_gwflow_AH: simulated groundwater flow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
29) prms_calib_sim_pref_flow_stor_AH: simulated storage (inches) in preferential flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
30) prms_calib_sim_slow_stor_AH: simulated storage (inches) in gravity flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
31) prms_calib_sim_soil_moist_AH: simulated storage (inches) in capillary flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
32) prms_calib_sim_soil_moist_tot_AH: total simulated soil zone storage (inches) from calibrated PRMS model in Arroyo Hondo subbasin
33) prms_calib_sim_hru_intcpstor_AH: simulated interception storage (inches) in the canopy from calibrated PRMS model in Arroyo Hondo subbasin
34) prms_calib_sim_hru_storage_AH: total simulated storage (inches) from calibrated PRMS model in Arroyo Hondo subbasin
35) prms_calib_sim_gwres_stor_AH: simulated storage (inches) in groundwater reservoir from calibrated PRMS model in Arroyo Hondo subbasin
36) prms_calib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin
37) prms_calib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin



Columns 38-41 are observed Upper Alameda subbasin variables
38) obs_precip_UA: observed precipitation (inches) in Upper Alameda subbasin
39) obs_tmaxc_UA: observed maximum temperature (degrees Celsius) in Upper Alameda subbasin
40) obs_tminc_UA: observed minimum temperature (degrees Celsius) in Upper Alameda subbasin
41) obs_flow_UA: observed streamflow (cfs) in Upper Alameda subbasin



Columns 42-57 are simulated Upper Alameda subbasin variables from an uncalibrated PRMS model
42) prms_uncalib_sim_flow_UA: simulated streamflow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
43) prms_uncalib_sim_actet_UA: simulated actual ET (inches) from uncalibrated PRMS model in Upper Alameda subbasin
44) prms_uncalib_sim_potet_UA: simulated potential ET (inches) from uncalibrated PRMS model in Upper Alameda subbasin
45) prms_uncalib_sim_swrad_UA: simulated shortwave radiation (Langleys) from uncalibrated PRMS model in Upper Alameda subbasin
46) prms_uncalib_sim_interflow_UA: simulated interflow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
47) prms_uncalib_sim_sroff_UA: simulated surface runoff (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
48) prms_uncalib_sim_gwflow_UA: simulated groundwater flow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
49) prms_uncalib_sim_pref_flow_stor_UA: simulated storage (inches) in preferential flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
50) prms_uncalib_sim_slow_stor_UA: simulated storage (inches) in gravity flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
51) prms_uncalib_sim_soil_moist_UA: simulated storage (inches) in capillary flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
52) prms_uncalib_sim_soil_moist_tot_UA: total simulated soil zone storage (inches) from uncalibrated PRMS model in Upper Alameda subbasin
53) prms_uncalib_sim_hru_intcpstor_UA: simulated interception storage (inches) in the canopy from uncalibrated PRMS model in Upper Alameda subbasin
54) prms_uncalib_sim_hru_storage_UA: total simulated storage (inches) from uncalibrated PRMS model in Upper Alameda subbasin
55) prms_uncalib_sim_gwres_stor_UA: simulated storage (inches) in groundwater reservoir from uncalibrated PRMS model in Upper Alameda subbasin
56) prms_uncalib_sim_dunnian_flow_UA: simulated Dunnian surface flow (inches) from uncalibrated PRMS model in Upper Alameda subbasin
57) prms_uncalib_sim_hortonian_flow_UA: simulated Hortonian surface flow (inches) from uncalibrated PRMS model in Upper Alameda subbasin



Columns 58-73 are simulated Upper Alameda subbasin variables from a calibrated PRMS model
58) prms_calib_sim_flow_UA: simulated streamflow (cfs) from calibrated PRMS model in Upper Alameda subbasin
59) prms_calib_sim_actet_UA: simulated actual ET (inches) from calibrated PRMS model in Upper Alameda subbasin
60) prms_calib_sim_potet_UA: simulated potential ET (inches) from calibrated PRMS model in Upper Alameda subbasin
61) prms_calib_sim_swrad_UA: simulated shortwave radiation (Langleys) from calibrated PRMS model in Upper Alameda subbasin
62) prms_calib_sim_interflow_UA: simulated interflow (cfs) from calibrated PRMS model in Upper Alameda subbasin
63) prms_calib_sim_sroff_UA: simulated surface runoff (cfs) from calibrated PRMS model in Upper Alameda subbasin
64) prms_calib_sim_gwflow_UA: simulated groundwater flow (cfs) from calibrated PRMS model in Upper Alameda subbasin
65) prms_calib_sim_pref_flow_stor_UA: simulated storage (inches) in preferential flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
66) prms_calib_sim_slow_stor_UA: simulated storage (inches) in gravity flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
67) prms_calib_sim_soil_moist_UA: simulated storage (inches) in capillary flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
68) prms_calib_sim_soil_moist_tot_UA: total simulated soil zone storage (inches) from calibrated PRMS model in Upper Alameda subbasin
69) prms_calib_sim_hru_intcpstor_UA: simulated interception storage (inches) in the canopy from calibrated PRMS model in Upper Alameda subbasin
70) prms_calib_sim_hru_storage_UA: total simulated storage (inches) from calibrated PRMS model in Upper Alameda subbasin
71) prms_calib_sim_gwres_stor_UA: simulated storage (inches) in groundwater reservoir from calibrated PRMS model in Upper Alameda subbasin
72) prms_calib_sim_dunnian_flow_UA: simulated Dunnian surface flow (inches) from calibrated PRMS model in Upper Alameda subbasin
73) prms_calib_sim_hortonian_flow_UA: simulated Hortonian surface flow (inches) from calibrated PRMS model in Upper Alameda subbasin



Columns 74-95 are simulated Arroyo Hondo subbasin variables from an uncalibrated SAC-SMA model
74) sacsma_uncalib_sim_effrain_AH: simulated effective rainfall (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
75) sacsma_uncalib_sim_uztwc_AH: simulated upper zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
76) sacsma_uncalib_sim_uzfwc_AH: simulated upper zone free water stroage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
77) sacsma_uncalib_sim_lztwc_AH: simulated lower zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
78) sacsma_uncalib_sim_lzfsc_AH: simulated lower zone secondary free water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
79) sacsma_uncalib_sim_lzfpc_AH: simulated lower zone primary free water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
80) sacsma_uncalib_sim_adimc_AH: additional impervious area water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
81) sacsma_uncalib_sim_sett_AH: simulated cumulative total evapotranspiration (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
82) sacsma_uncalib_sim_se1_AH: simulated cumulative evapotranspiration from upper zone tension water (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
83) sacsma_uncalib_sim_se3_AH: simulated cumulative evapotranspiration from lower zone tension water (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
84) sacsma_uncalib_sim_se4_AH: simulated cumulative evapotranspiration (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
85) sacsma_uncalib_sim_se5_AH: simulated cumulative evapotranspiration from riparian zone (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
86) sacsma_uncalib_sim_roimp_AH: simulated runoff from impervious area (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
87) sacsma_uncalib_sim_sdro_AH: simulated six hour sum of runoff (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
88) sacsma_uncalib_sim_ssur_AH: simulated surface runoff (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
89) sacsma_uncalib_sim_sif_AH: simulated interflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
90) sacsma_uncalib_sim_bfp_AH: simulated primary baseflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
91) sacsma_uncalib_sim_bfs_AH: simulated secondary baseflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
92) sacsma_uncalib_sim_bfcc_AH: simulated channel baseflow (bfp+bfs) (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
93) sacsma_uncalib_sim_slowflow_AH: simulated slow streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin 
94) sacsma_uncalib_sim_quickflow_AH: simulated quick streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
95) sacsma_uncalib_sim_flow_AH: simulated streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin



Columns 96-117 are simulated Arroyo Hondo subbasin variables from a calibrated SAC-SMA model
96) sacsma_calib_sim_effrain_AH: simulated effective rainfall (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
97) sacsma_calib_sim_uztwc_AH: simulated upper zone tension water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
98) sacsma_calib_sim_uzfwc_AH: simulated upper zone free water stroage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
99) sacsma_calib_sim_lztwc_AH: simulated lower zone tension water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
100) sacsma_calib_sim_lzfsc_AH: simulated lower zone secondary free water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
101) sacsma_calib_sim_lzfpc_AH: simulated lower zone primary free water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
102) sacsma_calib_sim_adimc_AH: additional impervious area water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
103) sacsma_calib_sim_sett_AH: simulated cumulative total evapotranspiration (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
104) sacsma_calib_sim_se1_AH: simulated cumulative evapotranspiration from upper zone tension water (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
105) sacsma_calib_sim_se3_AH: simulated cumulative evapotranspiration from lower zone tension water (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
106) sacsma_calib_sim_se4_AH: simulated cumulative evapotranspiration (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
107) sacsma_calib_sim_se5_AH: simulated cumulative evapotranspiration from riparian zone (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
108) sacsma_calib_sim_roimp_AH: simulated runoff from impervious area (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
109) sacsma_calib_sim_sdro_AH: simulated six hour sum of runoff (cfs) from an calibrated SAC-SMA model in Arroyo Hondo subbasin
110) sacsma_calib_sim_ssur_AH: simulated surface runoff (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
111) sacsma_calib_sim_sif_AH: simulated interflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
112) sacsma_calib_sim_bfp_AH: simulated primary baseflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
113) sacsma_calib_sim_bfs_AH: simulated secondary baseflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
114) sacsma_calib_sim_bfcc_AH: simulated channel baseflow (bfp+bfs) (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
115) sacsma_calib_sim_slowflow_AH: simulated slow streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
116) sacsma_calib_sim_quickflow_AH: simulated quick streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
117) sacsma_calib_sim_flow_AH: simulated streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin



Columns 118-139 are simulated Upper Alameda subbasin variables from an uncalibrated SAC-SMA model
118) sacsma_uncalib_sim_effrain_UA: simulated effective rainfall (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
119) sacsma_uncalib_sim_uztwc_UA: simulated upper zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
120) sacsma_uncalib_sim_uzfwc_UA: simulated upper zone free water stroage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
121) sacsma_uncalib_sim_lztwc_UA: simulated lower zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
122) sacsma_uncalib_sim_lzfsc_UA: simulated lower zone secondary free water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
123) sacsma_uncalib_sim_lzfpc_UA: simulated lower zone primary free water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
124) sacsma_uncalib_sim_adimc_UA: additional impervious area water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
125) sacsma_uncalib_sim_sett_UA: simulated cumulative total evapotranspiration (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
126) sacsma_uncalib_sim_se1_UA: simulated cumulative evapotranspiration from upper zone tension water (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
127) sacsma_uncalib_sim_se3_UA: simulated cumulative evapotranspiration from lower zone tension water (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
128) sacsma_uncalib_sim_se4_UA: simulated cumulative evapotranspiration (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
129) sacsma_uncalib_sim_se5_UA: simulated cumulative evapotranspiration from riparian zone (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
130) sacsma_uncalib_sim_roimp_UA: simulated runoff from impervious area (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
131) sacsma_uncalib_sim_sdro_UA: simulated six hour sum of runoff (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
132) sacsma_uncalib_sim_ssur_UA: simulated surface runoff (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
133) sacsma_uncalib_sim_sif_UA: simulated interflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
134) sacsma_uncalib_sim_bfp_UA: simulated primary baseflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
135) sacsma_uncalib_sim_bfs_UA: simulated secondary baseflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
136) sacsma_uncalib_sim_bfcc_UA: simulated channel baseflow (bfp+bfs) (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
137) sacsma_uncalib_sim_slowflow_UA: simulated slow streamflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
138) sacsma_uncalib_sim_quickflow_UA: simulated quick streamflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
139) sacsma_uncalib_sim_flow_UA: simulated streamflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin



Columns 140-161 are simulated Upper Alameda subbasin variables from a calibrated SAC-SMA model
140) sacsma_calib_sim_effrain_UA: simulated effective rainfall (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
141) sacsma_calib_sim_uztwc_UA: simulated upper zone tension water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
142) sacsma_calib_sim_uzfwc_UA: simulated upper zone tension water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
143) sacsma_calib_sim_lztwc_UA: simulated lower zone tension water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
144) sacsma_calib_sim_lzfsc_UA: simulated lower zone secondary free water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
145) sacsma_calib_sim_lzfpc_UA: simulated lower zone primary free water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
146) sacsma_calib_sim_adimc_UA: additional impervious area water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
147) sacsma_calib_sim_sett_UA: simulated cumulative total evapotranspiration (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
148) sacsma_calib_sim_se1_UA: simulated cumulative evapotranspiration from upper zone tension water (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
149) sacsma_calib_sim_se3_UA: simulated cumulative evapotranspiration from lower zone tension water (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
150) sacsma_calib_sim_se4_UA: simulated cumulative evapotranspiration (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
151) sacsma_calib_sim_se5_UA: simulated cumulative evapotranspiration from riparian zone (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
152) sacsma_calib_sim_roimp_UA: simulated runoff from impervious area (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
153) sacsma_calib_sim_sdro_UA: simulated six hour sum of runoff (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
154) sacsma_calib_sim_ssur_UA: simulated surface runoff (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
155) sacsma_calib_sim_sif_UA: simulated interflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
156) sacsma_calib_sim_bfp_UA: simulated primary baseflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
157) sacsma_calib_sim_bfs_UA: simulated secondary baseflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
158) sacsma_calib_sim_bfcc_UA: simulated channel baseflow (bfp+bfs) (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
159) sacsma_calib_sim_slowflow_UA: simulated slow streamflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
160) sacsma_calib_sim_quickflow_UA: simulated quick streamflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
161) sacsma_calib_sim_flow_UA: simulated streamflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin



Columns 162-167 are simulated Arroyo Hondo subbasin variables from an uncalibrated IHACRES model
162) ihacres_uncalib_sim_effrain_AH: simulated effective rainfall (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
163) ihacres_uncalib_sim_cmd_AH: simulated catchment moisture deficit (inches) from uncalibrated IHACRES model in Arroyo Hondo subbasin
164) ihacres_uncalib_sim_et_AH: simulated evapotranspiration (inches) from uncalibrated IHACRES model in Arroyo Hondo subbasin
165) ihacres_uncalib_sim_slowflow_AH: simulated slow streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
166) ihacres_uncalib_sim_quickflow_AH: simulated quick streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
167) ihacres_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin



Columns 168-173 are simulated Arroyo Hondo subbasin variables from a calibrated IHACRES model
168) ihacres_calib_sim_effrain_AH: simulated effective rainfall (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
169) ihacres_calib_sim_cmd_AH: simulated catchment moisture deficit (inches) from calibrated IHACRES model in Arroyo Hondo subbasin
170) ihacres_calib_sim_et_AH: simulated evapotranspiration (inches) from calibrated IHACRES model in Arroyo Hondo subbasin
171) ihacres_calib_sim_slowflow_AH: simulated slow streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
172) ihacres_calib_sim_quickflow_AH: simulated quick streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
173) ihacres_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin



Columns 174-179 are simulated Upper Alameda subbasin variables from an uncalibrated IHACRES model
174) ihacres_uncalib_sim_effrain_UA: simulated effective rainfall (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
175) ihacres_uncalib_sim_cmd_UA: simulated catchment moisture deficit (inches) from uncalibrated IHACRES model in Upper Alameda subbasin
176) ihacres_uncalib_sim_et_UA: simulated evapotranspiration (inches) from uncalibrated IHACRES model in Upper Alameda subbasin
177) ihacres_uncalib_sim_slowflow_UA: simulated slow streamflow (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
178) ihacres_uncalib_sim_quickflow_UA: simulated quick streamflow (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
179) ihacres_uncalib_sim_flow_UA: simulated streamflow (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin



Columns 180-185 are simulated Upper Alameda subbasin variables from a calibrated IHACRES model
180) ihacres_calib_sim_effrain_UA: simulated effective rainfall (cfs) from calibrated IHACRES model in Upper Alameda subbasin
181) ihacres_calib_sim_cmd_UA: simulated catchment moisture deficit (inches) from calibrated IHACRES model in Upper Alameda subbasin
182) ihacres_calib_sim_et_UA: simulated evapotranspiration (inches) from calibrated IHACRES model in Upper Alameda subbasin
183) ihacres_calib_sim_slowflow_UA: simulated slow streamflow (cfs) from calibrated IHACRES model in Upper Alameda subbasin
184) ihacres_calib_sim_quickflow_UA: simulated quick streamflow (cfs) from calibrated IHACRES model in Upper Alameda subbasin
185) ihacres_calib_sim_flow_UA: simulated streamflow (cfs) from calibrated IHACRES model in Upper Alameda subbasin



