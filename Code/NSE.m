%% computes NSE and decomposition
function [nse, alpha_nse, beta_nse, r] = NSE(model, observ)

    % compute mean and stdev
    mu_m = nanmean(model(:));
    mu_o = nanmean(observ(:));
    std_m = nanstd(model(:));
    std_o = nanstd(observ(:));
    
    % compute nse
    nse = 1 - nansum((model(:)-observ(:)).^2) / nansum((observ(:)-mu_o).^2);

    % compute decomposition
    alpha_nse = std_m / std_o;
    beta_nse = (mu_m - mu_o) / std_o;
    r = nansum((observ(:) - mu_o) .* (model(:) - mu_m)) / (nansum((observ(:) - mu_o).^2) * nansum((model(:) - mu_m).^2))^0.5;
end

