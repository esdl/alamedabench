%% computes mse, rmse, nrmse
function [mse, rmse, nrmse] = MSE(model, observ)
    mse = nanmean((model(:)-observ(:)).^2);
    rmse = mse.^0.5;
    nrmse = rmse ./ nanstd(observ(:));
end

