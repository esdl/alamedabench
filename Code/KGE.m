%% computes KGE and decomposition
function [kge, alpha_kge, beta_kge, r] = KGE(model, observ)
    mu_m = nanmean(model(:));
    mu_o = nanmean(observ(:));
    std_m = nanstd(model(:));
    std_o = nanstd(observ(:));
    
    alpha_kge = std_m / std_o;
    beta_kge = mu_m /  mu_o;
    r = nansum((observ(:) - mu_o) .* (model(:) - mu_m)) / (nansum((observ(:) - mu_o).^2) * nansum((model(:) - mu_m).^2))^0.5;
    kge = 1 - ((r-1)^2 + (alpha_kge-1)^2 + (beta_kge-1)^2)^0.5;
end
